package br.com.k21.cenarios;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.PendingException;
import cucumber.api.java.pt.Dado;

public class IMDbFilmSearchProcurarDiretor {

	WebDriver driver;
	
	@Dado("^que eu acessei a home do IMDB$")
	public void que_eu_acessei_a_home_do_IMDB() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "/home/lula/node_modules/chromedriver/chromedriver/chromedriver");
		driver = new ChromeDriver();
		driver.navigate().to("https://www.imdb.com");
	}

	@Dado("^digitei \"([^\"]*)\" no campo de busca$")
	public void digitei_no_campo_de_busca(String nomeFilme) throws Throwable {
		String seletorCampoBusca = "#navbar-query";
		WebElement campoDeBusca = driver.findElement(By.cssSelector(seletorCampoBusca));
		campoDeBusca.sendKeys(nomeFilme);
	}

	@Dado("^cliquei em buscar$")
	public void cliquei_em_buscar() throws Throwable {
		String seletorBotaoBusca = "#navbar-submit-button";
		WebElement botaoDeBusca = driver.findElement(By.cssSelector(seletorBotaoBusca));
		botaoDeBusca.click();
	}

}
