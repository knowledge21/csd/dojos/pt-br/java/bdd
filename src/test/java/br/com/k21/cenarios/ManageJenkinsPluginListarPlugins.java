package br.com.k21.cenarios;

import org.junit.AfterClass;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class ManageJenkinsPluginListarPlugins {

	WebDriver driver;

	@Dado("^que eu acessei a home do Jenkins$")
	public void que_eu_acessei_a_home_do_Jenkins() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "/home/lula/node_modules/chromedriver/chromedriver/chromedriver");
		driver = new ChromeDriver();
		driver.navigate().to("http://localhost:8080");
	}

	@Quando("^eu acessar a pagina de plugins instalados$")
	public void eu_acessar_a_pagina_de_plugins_instalados() throws Throwable {
		WebElement link = driver.findElement(By.cssSelector("#tasks > div:nth-child(6) > a.task-link"));
		link.click();
		
		link = driver.findElement(By.cssSelector("#main-panel > div:nth-child(9) > a > dl > dt"));
		link.click();
		
		link = driver.findElement(By.xpath("//*[@id=\"main-panel\"]/form/div[1]/div[1]/div[3]/a"));
		link.click();
	}

	@Entao("^o plugin \"([^\"]*)\" deve ser listado$")
	public void o_plugin_deve_ser_listado(String pluginName) throws Throwable {
		Assert.assertTrue(driver.getPageSource().contains(pluginName));
		driver.close();
		driver.quit();
	}
	
	@Dado("^o titulo \"([^\"]*)\" apareceu na pagina$")
	public void o_titulo_apareceu_na_pagina(String titulo) throws Throwable {
		WebElement elementoTitulo = driver.findElement(By.cssSelector("head > title"));
		String tituloApresentado = elementoTitulo.getText();
		Assert.assertEquals(titulo, tituloApresentado);
	}


}
